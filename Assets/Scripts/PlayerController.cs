﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float movementSpeed = 100f;
    public float runSpeed = 150f;

    private bool collided = false; 

    private void Update()
    {
        float speed = (Input.GetKey(KeyCode.LeftShift) ? runSpeed : movementSpeed);


        if (collided) return;

        if (Input.GetKey(KeyCode.W))
        {
            Vector3 vec = new Vector3(0,0, 0.1f * speed * Time.deltaTime);

            transform.Translate(vec);
        }

        if (Input.GetKey(KeyCode.S))
        {
            Vector3 vec = new Vector3(0, 0, -0.1f * speed * Time.deltaTime);

            transform.Translate(vec);
        }

        if (Input.GetKey(KeyCode.A))
        {
            Vector3 vec = new Vector3(-0.1f * speed * Time.deltaTime, 0, 0);

            transform.Translate(vec);
        }

        if (Input.GetKey(KeyCode.D))
        {
            Vector3 vec = new Vector3(0.1f * speed * Time.deltaTime, 0, 0);

            transform.Translate(vec);
        }



        //CHECK COLLISION MANUALLY
    }
}
