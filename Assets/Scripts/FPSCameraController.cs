﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCameraController : MonoBehaviour
{
    public float XSensivity = 2f;
    public float YSensivity = 2f;
    public float MinimumX = -90F;
    public float MaximumX = 90F;
    [SerializeField] private GameObject player;

    // Mouse 2 look
    // Easy 360° lateral, 180° vertical

    private void Awake()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        float yRot = Input.GetAxisRaw("Mouse X") * XSensivity;
        float xRot =  Input.GetAxisRaw("Mouse Y") * YSensivity;

        player.transform.localRotation *= Quaternion.Euler(0f, yRot, 0f);
        transform.localRotation *= Quaternion.Euler(-xRot, 0f, 0f);

        transform.localRotation = ClampRotationAroundXAxis(transform.localRotation);
    }

    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, MinimumX, MaximumX);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }

}
